" detect the os and whether if it's running with gui
let g:iswindows = 0
let g:islinux = 0
let g:ismac = 0
let g:isgui = 0

if (has("win32") || has("win64") || has("win95") || has("win16"))
	let g:iswindows = 1
elseif has("mac")
	let g:ismac = 1
else
	let g:islinux = 1
endif

if has("gui_running")
	let g:isgui = 1
endif

" reassign <leader> to ";" and ";" to "\"
" this should be the first setting.
let mapleader=";"
noremap \ ;

" plugin management
source $VIM/vimconfig/config/plugin_for_all.vim

" options
source $VIM/vimconfig/config/option.vim

" keymaps
source $VIM/vimconfig/config/keymap.vim

" abbreviation
source $VIM/vimconfig/config/abbrev.typo.vim
source $VIM/vimconfig/config/abbrev.short.vim

" functions
source $VIM/vimconfig/config/function.vim

" os dependently
if g:iswindows
	source $VIM/vimconfig/config/win.vim
endif

if g:islinux
	source $VIM/vimconfig/config/linux.vim
endif

if g:ismac
	source $VIM/vimconfig/config/mac.vim
endif
