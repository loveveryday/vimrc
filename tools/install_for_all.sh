#!/usr/bin/env sh

main() {
  # Use colors, but only if connected to a terminal, and that terminal
  # supports them.
  if which tput >/dev/null 2>&1; then
      ncolors=$(tput colors)
  fi
  if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
    RED="$(tput setaf 1)"
    GREEN="$(tput setaf 2)"
    YELLOW="$(tput setaf 3)"
    BLUE="$(tput setaf 4)"
    BOLD="$(tput bold)"
    NORMAL="$(tput sgr0)"
  else
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    NORMAL=""
  fi

  # Only enable exit-on-error after the non-critical colorization stuff,
  # which may fail on systems lacking tput or terminfo
  set -e

  # Prevent the cloned repository from having insecure permissions. Failing to do
  # so causes compinit() calls to fail with "command not found: compdef" errors
  # for users with insecure umasks (e.g., "002", allowing group writability). Note
  # that this will be ignored under Cygwin by default, as Windows ACLs take
  # precedence over umasks except for filesystems mounted with option "noacl".
  umask g-w,o-w

  hash git >/dev/null 2>&1 || {
    echo "Error: git is not installed"
    exit 1
  }

  hash vim >/dev/null 2>&1 || {
    echo "Error: vim is not installed"
    exit 1
  }

  hash ctags >/dev/null 2>&1 || {
    echo "Error: ctags is not installed"
    exit 1
  }

  # The Windows (MSYS) Git is not compatible with normal use on cygwin
  if [ "$OSTYPE" = cygwin ]; then
    if git --version | grep msysgit > /dev/null; then
      echo "Error: Windows/MSYS Git is not supported on Cygwin"
      echo "Error: Make sure the Cygwin git package is installed and is first on the path"
      exit 1
    fi
  fi

  VIM_CONFIG_DIR=/usr/share/vim/vimconfig
  VIM_RC_PATH=/etc/vimrc
  VIM_GRC_PATH=/etc/gvimrc

  printf "${BLUE}Looking for an existing vim config...${NORMAL}\n"
  now=`date +%Y%m%d%H%M%S`
  for i in $VIM_CONFIG_DIR $VIM_RC_PATH $VIM_GRC_PATH; do
    # printf "${RED}[$i]\n${NORMAL}"
    if [ -e "$i" ] || [ -h "$i" ]; then
      # printf "${RED}check file $i\n${NORMAL}"
      printf "${YELLOW}Found $i.${NORMAL} ${GREEN}Backing up to $i.$now.bakcup.\n${NORMAL}"
      mv $i $i.$now.backup;
    fi
  done

  printf "${BLUE}Cloning vimrc...${NORMAL}\n"
  env git clone --depth=1 https://gitlab.com/loveveryday/vimrc.git $VIM_CONFIG_DIR || {
    printf "Error: git clone of vimrc repo failed\n"
    exit 1
  }

  printf "${BLUE}Link basic.vim to vimrc...${NORMAL}\n"
  ln -s $VIM_CONFIG_DIR/basic_for_all.vim $VIM_RC_PATH

  printf "${BLUE}Cloning Vundle...${NORMAL}\n"
  env git clone --depth=1 https://github.com/gmarik/Vundle.vim.git $VIM_CONFIG_DIR/bundle/Vundle.vim || {
    printf "Error: git clone of Vundle repo failed\n"
    exit 1
  }

  printf "${BLUE}Installing Vim plugins using Vundle...${NORMAL}\n"
  vim +PluginInstall +qall

  printf "${GREEN}Vimrc is now installed, enjoying!${NORMAL}\n"
}

mkdir ~/.vim_backups
main
