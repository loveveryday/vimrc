#!/bin/sh

  # Use colors, but only if connected to a terminal, and that terminal
  # supports them.
  if which tput >/dev/null 2>&1; then
      ncolors=$(tput colors)
  fi
  if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
    RED="$(tput setaf 1)"
    GREEN="$(tput setaf 2)"
    YELLOW="$(tput setaf 3)"
    BLUE="$(tput setaf 4)"
    BOLD="$(tput bold)"
    NORMAL="$(tput sgr0)"
  else
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    NORMAL=""
  fi

  # Only enable exit-on-error after the non-critical colorization stuff,
  # which may fail on systems lacking tput or terminfo
  set -e

  # Prevent the cloned repository from having insecure permissions. Failing to do
  # so causes compinit() calls to fail with "command not found: compdef" errors
  # for users with insecure umasks (e.g., "002", allowing group writability). Note
  # that this will be ignored under Cygwin by default, as Windows ACLs take
  # precedence over umasks except for filesystems mounted with option "noacl".
  umask g-w,o-w

  hash patch >/dev/null 2>&1 || {
    printf "${RED}Error: patch is not installed${NORMAL}\n"
    exit 1
  }

printf "${BLUE}install oh-my-zsh as root${NORMAL}\n"
wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O /tmp/install.sh
chmod +x /tmp/install.sh
sed -i.1 '/^\s\+env zsh/d' /tmp/install.sh
sed -i.2 's/\(^\s\+exit$\)/\1 1/g' /tmp/install.sh
/tmp/install.sh
# source /tmp/install.sh

ret=$?
if [ $ret -ne 0 ]; then
  printf "${RED}oh-my-zsh install script return: $ret${NORMAL}\n"
  exit 1
fi

printf "${BLUE}Copy zsh files to /usr/share for all uer access${NORMAL}\n"
mv /root/.oh-my-zsh /usr/share/oh-my-zsh
rm /root/.zshrc

printf "${BLUE}Move into the dir and copy the zshrc template to zshrc (which will be the default for users)${NORMAL}\n"
cd /usr/share/oh-my-zsh/
# pwd
cp templates/zshrc.zsh-template zshrc

printf "${BLUE}Nab the patch file from MarcinWieczorek's AUR Package and apply to the zshrc file${NORMAL}\n"
wget https://aur.archlinux.org/cgit/aur.git/plain/0001-zshrc.patch\?h\=oh-my-zsh-git -O zshrc.patch && patch -p1 < zshrc.patch

printf "${BLUE}change default theme from \"robbyrussell\" to \"pygmalion\"${NORMAL}\n"
sed -i 's/robbyrussell/pygmalion/' zshrc
sed -i 's/mkdir $ZSH_CACHE_DIR/mkdir -p $ZSH_CACHE_DIR/' zshrc

printf "${BLUE}Create hard link to the zshrc file so it creates an actual independent copy on new users${NORMAL}\n"
mv /etc/skel/.zshrc /etc/skel/.zshrc.orig
ln /usr/share/oh-my-zsh/zshrc /etc/skel/.zshrc
cp /usr/share/oh-my-zsh/zshrc /root/.zshrc

printf "${BLUE}Set new user's default shell to zsh${NORMAL}\n"
adduser -D -s /bin/zsh

printf "${BLUE}Set oh-my-zsh for any pre-existing users${NORMAL}\n"
# ls /home
for user in `ls /home`; do
	if [ ! -e /home/$user/.oh-my-zsh ]; then
		printf "${YELLOW}change shell to zsh for $user${NORMAL}\n"
		cp /usr/share/oh-my-zsh/zshrc /home/$user/.zshrc
		chsh -s /bin/zsh $user
	fi
done

printf "${GREEN}oh-my-zsh setting done!${NORMAL}\n"

# change to zsh
cd - > /dev/null
env zsh
