There are user-specific and system-wide vim configurations. Normal we only need it user-specific, but sometimes we want it system-wide.

The scope is depend on it's location:

- user: ~/.vim, ~/.vimrc, ~/.gvimrc
- system: $VIM/vimfiles, $VIM/vimrc, $VIM/gvimrc

### Variables

- vimroot:   ~/.vim     or  $VIM/vimfiles
- vimfile:   ~/.vimrc   or  $VIM/vimrc
- vimgfile:  ~/.gvimrc  or  $VIM/gvimrc

#### *Vundle is used for plugin management.*
[Vundle](https://github.com/VundleVim/Vundle.vim)

### Prerequisites
- `curl` or `wget` should be installed
- `git` should be installed
- `ctags` should be installed

### Quick Installation

The configuration is installed by running one of the following commands in your terminal. You can install this via the command-line with either `curl` or `wget`.

#### via curl

- current user
  ```bash
  sh -c "$(curl -fsSL https://gitlab.com/loveveryday/vimrc/raw/master/tools/install.sh)"
  ```

- all user
  ```bash
  sh -c "$(curl -fsSL https://gitlab.com/loveveryday/vimrc/raw/master/tools/install_for_all.sh)"
  ```
#### via wget

- current user
  ```bash
  sh -c "$(wget https://gitlab.com/loveveryday/vimrc/raw/master/tools/install.sh -O -)"
  ```

- all user
  ```bash
  sh -c "$(wget https://gitlab.com/loveveryday/vimrc/raw/master/tools/install_for_all.sh -O -)"
  ```

### Install Manually

1. install Vundle in $VIM/vimfiles/bundle
   ```bash
   mkdir -p $vimroot/bundle
   git clone https://github.com/gmarik/Vundle.vim.git $vimroot/bundle/Vundle.vim
   ```

2. mv basic.vim to $vimfile
   ```bash
   cp basic.vim $vimfile
   ```

3. install plugin
   ```
   vi -c :PluginInstall, or
   vi --cmd :PluginInstall, or
   vi, in the vi interface, :PluginInstall
   ```

After that, all vim configs have been installed. Any user can be user the same config now.

### Install oh-my-zsh for all user

#### via curl
  ```bash
  sh -c "$(curl -fsSL https://gitlab.com/loveveryday/vimrc/raw/master/tools/install-oh-my-zsh.sh)"
  ```

#### via wget
  ```bash
  sh -c "$(wget https://gitlab.com/loveveryday/vimrc/raw/master/tools/install-oh-my-zsh.sh -O -)"
  ```