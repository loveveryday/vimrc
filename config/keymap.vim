" There are some keys such as <c-q>, <c-s> interpret by the terminal and
" therefore not reaching Vim. This can be fixed by adding

" silent !stty -ixon > /dev/null

" to .vimrc as it will disable XON/XOFF and then forces these control
" sequences to reach Vim.


""" key mapping

" map key for edit ~/.vimrc and source
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sb :w<cr>:source %:p<cr>

" remap the <c-p> and <c-n> so it can filter the command history
cnoremap <c-p> <up>
cnoremap <c-n> <down>

" insert current buffer's directory in command mode while type %%
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'

" make <c-l> can clear hight light search
nnoremap <c-l> :<c-u>nohls<cr><esc><c-l>

" count how many searchs
nnoremap <leader>n :%s///gn<cr>

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <c-u> <c-g>u<c-u>

" Fixing the & Command
nnoremap & :&&<cr>
xnoremap & :&&<cr>

" switch window
nnoremap [w <c-w><c-w>
nnoremap ]w <c-w><c-w>

" shortcut for quit
nnoremap <leader>q :q<cr>
nnoremap <leader>fq :q!<cr>
nnoremap <leader>wb :w<cr>
nnoremap <leader>wa :wa<cr>
nnoremap <leader>wq :wq<cr>

" shortcut for copy and paste
noremap <leader>y "+y
noremap <leader>p "+p
inoremap <c-y> <esc>"+pi

" shortcut for line beginning and ending
nnoremap <leader>la 0
nnoremap <leader>le $
nnoremap <c-p> <c-y>

" fast move in insert mode
inoremap <c-b> <left>
inoremap <c-f> <right>
inoremap <m-b> <esc>bi
inoremap <m-f> <esc>wi
inoremap <c-a> <home>
inoremap <c-e> <end>
inoremap <c-p> <up>
inoremap <c-n> <down>
inoremap <m-p> <pageup>
inoremap <m-n> <pagedown>
inoremap <c-o> <esc>o
inoremap <c-i> <c-o>
inoremap <c-s-o> <esc>O
inoremap <c-d> <delete>
