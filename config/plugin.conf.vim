""" Plungin Settings


"" YouCompleteme settings
" set completeopt=longest,menu	" let ycm complet act like ide
" close candidate window after left insert mode
" autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" let g:ycm_confirm_extra_conf=1
" let g:ycm_global_ycm_extra_conf = '~/workspace/gtk+/.ycm_extra_conf.py'
" let g:ycm_collect_identifiers_from_tags_files=1
" let g:ycm_seed_identifiers_with_syntax=1
" let g:ycm_complete_in_comments=1
" let g:ycm_complete_in_strings=1
" let g:ycm_collect_identifiers_from_comments_and_string=1
" let g:ycm_cache_omnifunc=0

"" NERDTree settings
nmap <leader>nt :NERDTree<cr>
let NERDTreeHighlightCursorline=1
let NERDTreeIgnore=[ '\.pyc$', '\.pyo$', '\.obj$', '\.o$', '\.so$', '\.egg$', '^\.git$', '^\.svn$', '^\.hg$' ]
let g:netrw_home='~/bak'

" close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | end

"" tagbar setting
nmap <leader>tb :TagbarToggle<cr>
let g:tagbar_autofocus=1

"" taglist settings
nmap <leader>tl :Tlist<cr><c-l>
let Tlist_Show_One_File=1
let Tlist_Exit_OnlyWindow=1
let Tlist_Use_Right_Window=1
let Tlist_File_Fold_Auto_Close=1

"" MiniBufExpl settings
" let g:miniBufExplMapWindowNavVim=1
" let g:miniBufExplMapWindowNavArrows=1
" let g:miniBufExplMapCTabSwitchBufs=1
" let g:miniBufExplModSelTarget=1
"
" let g:miniBufExplForceSyntaxEnable=1
" let g:miniBufExplorerMoreThanOne=2
" let g:minibufexplCycleArround=1
"
" map <s-tab> :MBEbn<cr>

"" ctrlp settings
let g:ctrlp_custom_ignore={
	\ 'dir': '\v[\/]\.(git|hg|svn|rvm)$',
	\ 'file': '\v\.(exe|so|dll|zip|tar|tar.gz)$'
	\ }
let g:ctrlp_working_path_mode=0
let g:ctrlp_match_window_bottom=1
let g:ctrlp_max_height=15
let g:ctrlp_match_window_reversed=0
let g:ctrlp_mruf_max=500
let g:ctrlp_follow_symlinks=1
" let g:ctrlp_map='<leader>ff'
" let g:ctrlp_cmd='CtrlP'

" map <leader>fp :CtrlPMRU<cr>

"" vim-powerline settings
" let g:Powerline_symbols='unicode'
" let g:Powerline_stl_path_style = 'filename'
" if g:isterm
	" set t_Co=256 " enable the colored statuslne for powerline
" endif

"" vim-indent-guides settings
let g:indent_guides_enable_on_vim_startup = 0
let g:indent_guides_guide_size            = 1
let g:indent_guides_start_level			  = 2

"" wim-trailing-whitespace settings
noremap <silent> <leader><space> :FixWhitespace<cr>

"" ultisnips settings
let g:UltiSnipsExpandTrigger = "<c-j>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

"" nerdcommenter settings
let NERDSpaceDelims = 1

"" delimitMate settings
au FileType python let b:delimitMate_nesting_quotes = ['"']

"" tabular settings
nmap <leader>bb :Tab /=<cr>
nmap <leader>bn :Tab /

"" syntastic settings
" let g:syntastic_error_symbol = 'â'	"set error or warning signs
" let g:syntastic_warning_symbol = 'â '
" let g:syntastic_check_on_open=1
" let g:syntastic_enable_highlighting = 0
"let g:syntastic_python_checker="flake8,pyflakes,pep8,pylint"
" let g:syntastic_python_checkers=['pyflakes']
" highlight SyntasticErrorSign guifg=white guibg=black

" let g:syntastic_cpp_include_dirs = ['/usr/include/']
" let g:syntastic_cpp_remove_include_errors = 1
" let g:syntastic_cpp_check_header = 1
" let g:syntastic_cpp_compiler = 'clang++'
" let g:syntastic_cpp_compiler_options = '-std=c++11 -stdlib=libstdc++'
" let g:syntastic_enable_balloons = 1	"whether to show balloons

"" TaskList settings
map <leader>td <plug>TaskList


"" ack
" let g:ackprg = 'ag --nogroup --nocolor --column'
let g:ackprg = 'ag --vimgrep'
