"" Vundle Plugin Management
set nocompatible	" be iMproved, required
filetype off		" required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=~/.vim/bundle/fzf
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" YouCompleteme
" Plugin 'Valloric/YouCompleteme'

" color scheme
Plugin 'altercation/vim-colors-solarized'
Plugin 'tomasr/molokai'

" NERD tree
Plugin 'scrooloose/nerdtree'

" tagbar
Plugin 'majutsushi/tagbar'

" taglist
Plugin 'vim-scripts/taglist.vim'

" minibufexpl
" Plugin 'fholgado/minibufexpl'

" ctrlp
Plugin 'ctrlpvim/ctrlp.vim'

" vim-powerline
" Plugin 'Lokaltog/vim-powerline'

" parentheses
Plugin 'kien/rainbow_parentheses.vim'

" vim-indent-guides
Plugin 'nathanaelkane/vim-indent-guides'

" vim-trailing-whitespace
Plugin 'bronson/vim-trailing-whitespace'

" vim-easymotion
Plugin 'Lokaltog/vim-easymotion'

" matchit
Plugin 'vim-scripts/matchit.zip'

" ultisnips
Plugin 'SirVer/ultisnips'

" vim-snippets
Plugin 'honza/vim-snippets'

" nerdcommenter
Plugin 'scrooloose/nerdcommenter'

" vim-surround
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-repeat'

" delimitMate
Plugin 'Raimondi/delimitMate'

" tabular
Plugin 'godlygeek/tabular'

" syntastic
Plugin 'scrooloose/syntastic'

" TaskList
Plugin 'vim-scripts/TaskList.vim'

" unimpaired
Plugin 'tpope/vim-unimpaired'

" rails
Plugin 'tpope/vim-rails'

" header and source switch
Plugin 'a.vim'

" textobj-lastpat
" Plugin 'kana/vim-textobj-lastpat'

" abolish
Plugin 'tpope/vim-abolish'

" qargs
Plugin 'nelstrom/vim-qargs'

" ack
Plugin 'mileszs/ack.vim'

" fugitive
Plugin 'tpope/vim-fugitive'

" DrawIt
Plugin 'DrawIt'

" vim wiki
Plugin 'vimwiki'

" vim VisIncr
Plugin 'VisIncr'

" emmet
Plugin 'mattn/emmet-vim'

" autocorrect
Plugin 'panozzaj/vim-autocorrect'

" vim-renamer
Plugin 'qpkorr/vim-renamer'

" fzf
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'

" All of your Plugins must be added before the following line
call vundle#end()			" required
filetype plugin indent on	" required
" To ignore plugin indent changes, instead use:
" filetype plugin on
"
" Brief help
" :PluginList		- list configured plugins
" :PluginInstall(!)	- install (update) plugins
" :PluginSearch(!) foo	- search (or refresh cache first) for foo
" :PluginClean(!)	- confirm (or auto-approve) removal of unused plugins
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


"" Include plugins config
source ~/.vim/config/plugin.conf.vim
