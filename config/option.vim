" allow backspacing over everything in insert mode
set backspace=indent,eol,start

if has("vms")
  set nobackup	" do not keep a backup file, use versions instead
else
  set backup	" keep a backup file
  " set writebackup " make backup before overwriting the current buffe
  set backupdir=~/.vim_backups
  au BufWritePre * let &backupext = '@' . substitute(expand("%:p:h"), "/" , "%" , "g") . "@" . strftime("%Y.%m.%d.%H.%M.%S")
  "  &backupext = &bex
  " last `/` in path `~/.vim_backups//` means dir path
  "  backupdir need create manually
  " set backupdir=~/.vim_backups//
  " au BufWritePre * let &bex = '@' . strftime("%F.%H.%M.%S")
endif

" In many terminal emulators the mouse works just fine, thus enable it.
" if has('mouse')
"   set mouse=a
" endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  " filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  " autocmd FileType text setlocal textwidth=78

  " For all tex files xelatex it on buffer write
  " autocmd BufWritePost *.tex !xelatex expand('%')

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

""" Load the vim script shipped with vim
runtime! ftplugin/man.vim

""" options
" if set number is on, increase the columns
set number relativenumber
set smartindent
set tabstop=4
set shiftwidth=4
set softtabstop=4
" set expandtab
set showmatch
set laststatus=2
set autowrite
set history=500 " keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch	" do incremental searching

" file encoding, support chinese
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=ucs-bom,utf-8,gb18030,gbk,gb2312,cp936,latin1

if g:isgui
	" hide menu, toolbar, scrollbar
	" set go-=T
	" set go-=m
	" set go-=r
	" set go-=L

	" hilight current cursor in line and column on gui only
	set cursorline
	set cursorcolumn

	" pick a color scheme
	colorscheme solarized
	set bg=dark
endif
