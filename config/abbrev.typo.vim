" we always have some common typos, it's useful to put them here.
" when found typo while typing with spellchecker, we can add it
" here immediately.
iabbrev adn and
iabbrev waht what
iabbrev tehn then
iabbrev teh the
iabbrev het the
