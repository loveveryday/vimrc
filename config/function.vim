" 实现在下一行与当前行前一个空白处相同位置插入，方便进行列操作。如：
    " row1-col1      row1-col2       row1-clo3
    " row2-col1
    " row3-col1
    " 当光标位于 row1-col2 后面的时候，通过快捷键可以在第二行与 row1-col2 的 r 相同的位置插入。
" tab/jump out of brackets
" func skip_pairs()
	" let char = getline('.')[col('.') - 1]
	" if char == ')' || char == ']' || char == '}' || char == '""' || char == "'"
	" if getline('.')[col('.') - 1] == ')' || getline('.')[col('.') - 1] == ']' || getline('.')[col('.') - 1] == '}' || getline('.')[col('.') - 1] == '"' || getline('.')[col('.') - 1] == "'"
		 " return "\<ESC>la"
	 " else
		 " return "\t"
	 " endif
 " endfunc

" inoremap <TAB> <c-r>=skip_pairs()<cr>
" inoremap <expr> <TAB> stridx('])}', getline('.')[col('.')-1])==-1 ? "\t" : "\<Right>"
" it's won't work because tab assign to auto complete by ycm.
