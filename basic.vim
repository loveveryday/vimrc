" detect the os and whether if it's running with gui
let g:iswindows = 0
let g:islinux = 0
let g:ismac = 0
let g:isgui = 0

if (has("win32") || has("win64") || has("win95") || has("win16"))
	let g:iswindows = 1
elseif has("mac")
	let g:ismac = 1
else
	let g:islinux = 1
endif

if has("gui_running")
	let g:isgui = 1
endif

" reassign <leader> to ";" and ";" to "\"
" this should be the first setting.
let mapleader=";"
noremap \ ;

" plugin management
source ~/.vim/config/plugin.vim

" options
source ~/.vim/config/option.vim

" keymaps
source ~/.vim/config/keymap.vim

" abbreviation
source ~/.vim/config/abbrev.typo.vim
source ~/.vim/config/abbrev.short.vim

" functions
source ~/.vim/config/function.vim

" os dependently
if g:iswindows
	source ~/.vim/config/win.vim
endif

if g:islinux
	source ~/.vim/config/linux.vim
endif

if g:ismac
	source ~/.vim/config/mac.vim
endif
